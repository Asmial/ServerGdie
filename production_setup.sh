#!/usr/bin/env bash

cd app
echo "Obteniendo los paquetes necesarios"
npm install
echo "Generando ficheros del cliente"
node build-client.js
echo "Ficheros generados"
cd ..
echo "Iniciando servidor"
docker-compose up -d
echo "Servidor iniciado, para pararlo usar: docker-compose down"